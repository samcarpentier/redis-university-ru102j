#!/bin/bash

ACTION=$1

if [ "$ACTION" == "build" ]; then
  mvn package -DskipTests=true
fi

if [ "$ACTION" == "load-data" ]; then
  java -jar target/redisolar-1.0.jar load --flush true
fi

if [ "$ACTION" == "run" ]; then
  java -jar target/redisolar-1.0.jar server config.yml
fi
